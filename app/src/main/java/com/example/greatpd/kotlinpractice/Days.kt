package com.example.greatpd.kotlinpractice

import java.time.DayOfWeek

//2016년 1월 1일은 금요일입니다. 2016년 a월 b일은 무슨 요일일까요? 두 수 a ,b를 입력받아 2016년 a월 b일이 무슨 요일인지 리턴하는 함수, solution을 완성하세요. 요일의 이름은 일요일부터 토요일까지 각각 SUN,MON,TUE,WED,THU,FRI,SAT
//
//입니다. 예를 들어 a=5, b=24라면 5월 24일은 화요일이므로 문자열 TUE를 반환하세요.
//
//제한 조건
//2016년은 윤년입니다.
//2016년 a월 b일은 실제로 있는 날입니다. (13월 26일이나 2월 45일같은 날짜는 주어지지 않습니다)
//입출력 예
//a	b	result
//5	24	TUE


// 1,3,5,7,8,10,12 => 31일 => +3 같은일
// 4,6,9,11 => 30일 => +2 같은일
// 2 => 윤년 => 29 => +1
val monthPlus = intArrayOf(3,1,3,2,3,2,3,3,2,3,2,3)
// 문자열 배열 arrayOf
val dayArray = arrayOf("SUN","MON","TUE","WED","THU","FRI","SAT")

fun main() {
    solutionss(1,3)
}

fun solutionss(a: Int, b: Int): String {
    var answer = ""
    var plusSum = 0
    for((index,plus) in monthPlus.withIndex()){
       if(index < a-1){
           plusSum = plusSum+plus
       }
    }
    // a/1 까지 더해저야 할 인덱스
    var totalPlusSum = plusSum%7

    // a/1 요일의 요일 계산
    var gizunDay = dayArray.indexOf("FRI") // 금요일
    var a_1DaysIndex = 0

    if(totalPlusSum > 1){
        a_1DaysIndex = ((gizunDay + totalPlusSum)%6)-1
    }else{
        a_1DaysIndex = (gizunDay + totalPlusSum)
    }

    //println("${a}/1의 요일 ====> ${dayArray[a_1DaysIndex]}")

    // a/b 의 의 요일 계산
    // a/1 기준 남은일자 % 7 의 나머지
    var extraDays = (b-1)%7
    // a/1 기준 인덱스 남은일자 +할 요일 인덱스
    var a_bDays = a_1DaysIndex + extraDays

    // 인덱스 넘어갔을때
    if(a_bDays>=7){
      answer = dayArray[(a_bDays%6)-1]
    }else{
      answer = dayArray[a_bDays]
    }

    println("${a}/${b}의 요일 ====> ${answer}")

    return answer
}

