package com.example.greatpd.kotlinpractice

fun main() {
    println("테스트")
    showYaksu(5)
}

fun showYaksu(jungSu : Int){
    // 약수 10 - 1,2,5,10
    val yaksuArray = ArrayList<Int>()
    // 10-> 1~10까지  0 나누어 떨어지는 경우
    for (i in 1..jungSu){
        if(jungSu % i == 0){
            yaksuArray.add(i)
        }
    }

    println("${yaksuArray}")
}

