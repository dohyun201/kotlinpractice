package com.example.greatpd.kotlinpractice

//단어 s의 가운데 글자를 반환하는 함수, solution을 만들어 보세요. 단어의 길이가 짝수라면 가운데 두글자를 반환하면 됩니다.
//
//재한사항
//s는 길이가 1 이상, 100이하인 스트링입니다.
//입출력 예
//s	return
//abcde	c
//qwer   we

fun main() {
    solutionGet("pyun")
}

fun solutionGet(getStr: String): String {
    // 길이%2 = 0 =>짝 => 두글자 abcdef /2 의 몫  + 몫-1
    // 길이%2 = 1 =>홀 => 가운데글자 /2 의 몫의 번째
    var answer = ""

    if(getStr.length in 1..100){
        
        var mediumIndex = getStr.length/2

        when(getStr.length%2){
            1 -> answer = getStr.get(mediumIndex).toString()
            0 -> answer = getStr.get(mediumIndex-1) + "" + getStr.get(mediumIndex)
        }

        println("getString = ${answer}")
    }
    
    return answer
}

//class Solution {
//    fun solution(s: String): String {
//        var answer = ""
//        return answer
//    }
//}