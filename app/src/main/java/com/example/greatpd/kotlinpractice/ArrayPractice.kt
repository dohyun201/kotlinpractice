package com.example.greatpd.kotlinpractice

import org.json.JSONArray
import org.json.JSONObject

val answerList = intArrayOf(1,2,3,4,5)
val questionArray = onePattern()
var personArray = arrayOf("A","B","C")

fun main() {

    val personOneAnswerCount = checkCurrect(onePattern())
    val personTwoAnswerCount = checkCurrect(twoPattern())
    val personThreeAnswerCount = checkCurrect(threePattern())

    val rankDescArray = intArrayOf(personOneAnswerCount,personTwoAnswerCount,personThreeAnswerCount).sortedArrayDescending()
    for (i in 0..rankDescArray.size-1){
        println("Person:"+personArray[i]+"\n 점수:"+rankDescArray.get(i)/100+"점")
    }
}


fun onePattern() : ArrayList<Int>{
    val onePattern :ArrayList<Int> = ArrayList()

    for(i in 0..9999){
        if(i<5){
            onePattern.add(answerList[i])
        }
        else{
            onePattern.add(answerList[i%5])
        }
    }
    return onePattern
}

fun twoPattern() : ArrayList<Int>{
    val twoPattern : ArrayList<Int> = ArrayList()
    for(i in 0..9999){
        if(i%2 == 0){
            twoPattern.add(answerList[1])
        }else{
            twoPattern.add(answerList[i%5])
        }
    }
    return twoPattern
}

fun threePattern() : ArrayList<Int>{
    val threePattern : ArrayList<Int> = ArrayList()
    for (i in 1..10000){
        if(i<5){
            if(i%2==1){
                threePattern.add(answerList[i])
            }else{
                threePattern.add(answerList[i-1])
            }
        }else{
            //5
            if(i%2==1){
                threePattern.add(answerList[i%5])
            }else{
                threePattern.add(answerList[(i-1)%5])
            }
        }
    }
    return threePattern
}

fun checkCurrect(answerArray : ArrayList<Int>): Int{
    var currectCount = 0

    for(i in 0..9999){
        if(questionArray.get(i) == answerArray.get(i)){
            currectCount++
        }
    }
    return currectCount
}