package com.example.greatpd.kotlinpractice

/* 문제
배열 array의 i번째 숫자부터 j번째 숫자까지 자르고 정렬했을 때, k번째에 있는 수를 구하려 합니다.

예를 들어 array가 [1, 5, 2, 6, 3, 7, 4], i = 2, j = 5, k = 3이라면

array의 2번째부터 5번째까지 자르면 [5, 2, 6, 3]입니다.
1에서 나온 배열을 정렬하면 [2, 3, 5, 6]입니다.
2에서 나온 배열의 3번째 숫자는 5입니다.

배열 array, [i, j, k]를 원소로 가진 2차원 배열 commands가 매개변수로 주어질 때, commands의 모든 원소에 대해 앞서 설명한 연산을 적용했을 때 나온 결과를 배열에 담아 return 하도록 solution 함수를 작성해주세요.

제한사항
array의 길이는 1 이상 100 이하입니다.
array의 각 원소는 1 이상 100 이하입니다.
commands의 길이는 1 이상 50 이하입니다.
commands의 각 원소는 길이가 3입니다.
입출력 예
array	commands	return
[1, 5, 2, 6, 3, 7, 4]	[[2, 5, 3], [4, 4, 1], [1, 7, 3]]	[5, 6, 3]
입출력 예 설명
[1, 5, 2, 6, 3, 7, 4]를 2번째부터 5번째까지 자른 후 정렬합니다. [2, 3, 5, 6]의 세 번째 숫자는 5입니다.
[1, 5, 2, 6, 3, 7, 4]를 4번째부터 4번째까지 자른 후 정렬합니다. [6]의 첫 번째 숫자는 6입니다.
[1, 5, 2, 6, 3, 7, 4]를 1번째부터 7번째까지 자릅니다. [1, 2, 3, 4, 5, 6, 7]의 세 번째 숫자는 3입니다.*/

val intsArrayOf : IntArray = intArrayOf(1,2,3)

    fun solution(array: IntArray, commands: Array<IntArray>): IntArray {

        println("checkArraySize(array) = ${checkArraySize(array)}")
        println("checkCommandSize(commands) = ${checkCommandSize(commands)}")

        if(checkArraySize(array) && checkCommandSize(commands)){
           println("둘다 true!")
           sortArrays(array,commands);
        }

        var answer = intArrayOf()
        answer = intsArrayOf
//        println("responseValueArray = ${answer}")
        return answer
    }

fun sortArrays(array: IntArray, commands: Array<IntArray>) {
    var commandOne = -1
    var commandTwo = -1
    var commandThree = -1

    // 커맨드1 시작점,
    for((v,i) in commands.withIndex()){
        for(j in i){
            // command 1,2,3 세팅
            if(commandOne == -1){
                commandOne = j
            }else if(commandTwo == -1){
                commandTwo = j
            }else{
                commandThree = j
            }
        }

        // 커맨드2 종료점, 정렬
        var newArray  = array.slice(commandOne-1..commandTwo-1).sorted()
        // 커맨드 3 그떄에 해당하는 숫자
        var responseValue = newArray[commandThree-1]

        println("${array.slice(0..array.size-1)} 를 ${commandOne} 번쨰 부터 ${commandTwo} 까지 자른후 정렬합니다. ${newArray} 의  ${commandThree}번째 숫자는 ${responseValue} 입니다.")

        // 초기화
        commandOne = -1
        commandTwo = -1
        commandThree = -1

        intsArrayOf.set(v,responseValue)
    }
}

fun checkCommandSize(commands: Array<IntArray>): Boolean {
    //commands의 길이는 1 이상 50 이하입니다.
    //commands의 각 원소는 길이가 3입니다.
    var booleanValue = false

    if(commands.size in 1..50){
        booleanValue = true
    }

    for(i in commands){
        if(i.size == 3){
            booleanValue = true
        }else{
            booleanValue = false
        }
    }
    return booleanValue
}

fun checkArraySize(array: IntArray) :Boolean{
    var booleanValue = false

//    array의 길이는 1 이상 100 이하입니다.
//    array의 각 원소는 1 이상 100 이하입니다.

    if(array.size in 1..100){
        for(i in array){
            if(i in 1..100){
                booleanValue = true
            }
        }
    }else{
        booleanValue =  false
    }
    return booleanValue
}

fun main() {
        val inputArray = intArrayOf(1,5,2,6,3,7,4)
        val firstIntArray = intArrayOf(2,5,3)
        val secondIntArray = intArrayOf(4,4,1)
        val thirdIntArray = intArrayOf(1,7,3)
        val inputArrayCommand:Array<IntArray> = arrayOf(firstIntArray,secondIntArray,thirdIntArray)

//        solution(inputArray,inputArrayCommand)
        println("solution() = ${solution(inputArray,inputArrayCommand).contentToString()}")
    }




